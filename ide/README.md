# IDE files

## Visual Studio Code

- Make sure your toolchain root is at ``toolchain/`` in your workspace root (subject to change).
- Copy the [vscode/c_cpp_properties.json]() into the ``.vscode`` directory in your workspace root.
- Select ``Freestanding`` as ``C/C++ Configuration``