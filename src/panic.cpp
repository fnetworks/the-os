#include "panic.hpp"
#include "asm.hpp"
#include "vga_term.hpp"

__attribute__((noreturn)) void panic() {
	while (true) {
		iasm::cli();
		iasm::hlt();
	}
}

__attribute__((noreturn)) void panic(const char* msg) {
	if (msg) {
		auto term = VGATerminal::current();
		if (term->is_available()) {
			term->put_string("*** KERNEL PANIC ***\n");
			term->put_string(msg);
			term->new_line();
		}
	}
	panic();
}