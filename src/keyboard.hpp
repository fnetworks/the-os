#pragma once

#include <stdint.h>
#include "memory.hpp"
#include "printf.hpp"
#include "port.hpp"
#include "string.hpp"

char translate_scancode(int set, bool release, uint16_t scancode) {
	static bool shift = false;
	static bool ctrl = false;
	static bool alt = false;
	static bool alt_gr = false;

	if (scancode == 0x2a || scancode == 0x36) {
		shift = !release;
		return '\0';
	} else if (scancode == 0x1d) {
		ctrl = !release;
		return '\0';
	} else if (scancode == 0x38) {
		if (set == 0)
			alt = !release;
		else if (set == 1)
			alt_gr = !release;
		return '\0';
	}

	if (release) {
		return '\0';
	}

	if (shift) {
		switch (scancode) {
			case 0x02: return '!';
			case 0x03: return '"';
			case 0x04: return '\0'; // '§' not displayable
			case 0x05: return '$';
			case 0x06: return '%';
			case 0x07: return '&';
			case 0x08: return '/';
			case 0x09: return '(';
			case 0x0a: return ')';
			case 0x0b: return '=';
			case 0x0c: return '?';
			case 0x10: return 'Q';
			case 0x11: return 'W';
			case 0x12: return 'E';
			case 0x13: return 'R';
			case 0x14: return 'T';
			case 0x15: return 'Z';
			case 0x16: return 'U';
			case 0x17: return 'I';
			case 0x18: return 'O';
			case 0x19: return 'P';
			case 0x1b: return '*';
			case 0x1e: return 'A';
			case 0x1f: return 'S';
			case 0x20: return 'D';
			case 0x21: return 'F';
			case 0x22: return 'G';
			case 0x23: return 'H';
			case 0x24: return 'J';
			case 0x25: return 'K';
			case 0x26: return 'L';
			case 0x2b: return '\'';
			case 0x2c: return 'Y';
			case 0x2d: return 'X';
			case 0x2e: return 'C';
			case 0x2f: return 'V';
			case 0x30: return 'B';
			case 0x31: return 'N';
			case 0x32: return 'M';
			case 0x33: return ';';
			case 0x34: return ':';
			case 0x35: return '_';
			case 0x56: return '>';
		}
	}
	if (alt_gr) {
		switch (scancode) {
			case 0x08: return '{';
			case 0x09: return '[';
			case 0x0a: return ']';
			case 0x0b: return '}';
			case 0x0c: return '\\';
			case 0x10: return '@';
			case 0x1b: return '~';
			case 0x56: return '|';
		}
	}
	switch (scancode) {
		case 0x02: return '1';
		case 0x03: return '2';
		case 0x04: return '3';
		case 0x05: return '4';
		case 0x06: return '5';
		case 0x07: return '6';
		case 0x08: return '7';
		case 0x09: return '8';
		case 0x0a: return '9';
		case 0x0b: return '0';
		case 0x0f: return '\t';
		case 0x10: return 'q';
		case 0x11: return 'w';
		case 0x12: return 'e';
		case 0x13: return 'r';
		case 0x14: return 't';
		case 0x15: return 'z';
		case 0x16: return 'u';
		case 0x17: return 'i';
		case 0x18: return 'o';
		case 0x19: return 'p';
		case 0x1b: return '+';
		case 0x1c: return '\n';
		case 0x1e: return 'a';
		case 0x1f: return 's';
		case 0x20: return 'd';
		case 0x21: return 'f';
		case 0x22: return 'g';
		case 0x23: return 'h';
		case 0x24: return 'j';
		case 0x25: return 'k';
		case 0x26: return 'l';
		case 0x2b: return '#';
		case 0x2c: return 'y';
		case 0x2d: return 'x';
		case 0x2e: return 'c';
		case 0x2f: return 'v';
		case 0x30: return 'b';
		case 0x31: return 'n';
		case 0x32: return 'm';
		case 0x33: return ',';
		case 0x34: return '.';
		case 0x35: return '-';
		case 0x39: return ' ';
		case 0x56: return '<';
	}

	// printf("Scancode: %h/%h\n", set, scancode);

	return '\0';
}

void handle_keyboard_irq() {
	Port<uint8_t> kbd_port(0x60);
	uint8_t scancode = kbd_port.read();

	static uint32_t e0_code = 0;
	static uint32_t e1_code = 0;
	static uint32_t e1_prev = 0;
	
	static char linebuf[200];
	static uint16_t line_index = 0;

	bool release = false;
	char keycode = 0;

	if ((scancode & 0x80) && (e1_code || (scancode != 0xE1)) && (e0_code || (scancode != 0xE0))) {
		release = true;
		scancode &= ~0x80;
	}

	if (e0_code) {
		if ((scancode == 0x2A) || (scancode == 0x36)) {
			e0_code = 0;
			return;
		}

		keycode = translate_scancode(1, release, scancode);
		e0_code = 0;
	} else if (e1_code == 2) {
		e1_prev |= ((uint16_t)scancode << 8);
		keycode = translate_scancode(2, release, e1_prev);
		e1_code = 0;
	} else if (scancode == 0xE0) {
		e0_code = 1;
	} else if (scancode == 0xE1) {
		e1_code = 1;
	} else {
		keycode = translate_scancode(0, release, scancode);
	}

	if (keycode != 0) {
		printf("%c", keycode);
	
		if (keycode == '\n') {
			line_index = 0;
			if (strcmp(linebuf, "shutdown") == 0) {
				Port<uint16_t>(0x604).write(0x2000);
			} else if (strcmp(linebuf, "help") == 0) {
				println("Commands:");
				println("\thelp");
				println("\tshutdown (QEMU only)");
			} else {
				printf("Unknown command %s\n", linebuf);
			}
			memset(linebuf, 0, sizeof(linebuf));
		} else {
			if (line_index >= 279) return;
			linebuf[line_index] = keycode;
			line_index++;
		}
	}
}