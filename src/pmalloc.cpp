#include "pmalloc.hpp"

#define bitsizeof(x) (sizeof(x) * 8)

extern char kernel_start;
extern char kernel_end;

#include "printf.hpp"

namespace pmm {

	constexpr size_t BLOCK_SIZE = 0x1000;
	constexpr size_t BLOCK_COUNT = 0x100000;

	using bitmap_type = uint32_t;
	static bitmap_type bitmap[BLOCK_COUNT / bitsizeof(bitmap_type)];

	void init(const multiboot::Info* mb_info) {
		const auto* mmap = reinterpret_cast<const multiboot::MMapEntry*>(mb_info->mmap_addr);
		const auto* const mmap_end =
			reinterpret_cast<const multiboot::MMapEntry*>(mb_info->mmap_addr + mb_info->mmap_length);
		for (; mmap < mmap_end; mmap++) {
			if (mmap->type == multiboot::MMapEntry::Type::Available) {
				uintptr_t addr = static_cast<uintptr_t>(mmap->addr);
				uintptr_t end_addr = static_cast<uintptr_t>(addr + mmap->len);
				for (; addr < end_addr; addr += BLOCK_SIZE)
					free(reinterpret_cast<void*>(addr));
			}
		}

		for (const char* kernel_addr = &kernel_start; kernel_addr < &kernel_end; kernel_addr += BLOCK_SIZE) {
			mark_reserved(kernel_addr);
		}

		mark_reserved(mb_info);

		mark_reserved(nullptr);
	}

	void* allocate() {
		for (size_t i = 0; i < BLOCK_COUNT / bitsizeof(bitmap_type); i++) {
			for (uint8_t bit = 0; bit < bitsizeof(bitmap_type); bit++) {
				auto block_free = (bitmap[i] >> bit) & 0x1;
				if (block_free == 1) {
					bitmap[i] &= ~(1 << bit);
					return reinterpret_cast<void*>((i * bitsizeof(bitmap_type) + bit) * BLOCK_SIZE);
				}
			}
		}
		return nullptr;
	}

	void mark_reserved(const void* address) {
		uintptr_t addr = reinterpret_cast<uintptr_t>(address) / BLOCK_SIZE;

		size_t offset = addr / bitsizeof(bitmap_type);
		size_t bit_offset = addr % bitsizeof(bitmap_type);

		bitmap[offset] &= ~(1 << bit_offset);
	}

	void free(void* address) {
		uintptr_t addr = reinterpret_cast<uintptr_t>(address) / BLOCK_SIZE;

		size_t offset = addr / bitsizeof(bitmap_type);
		size_t bit_offset = addr % bitsizeof(bitmap_type);

		bitmap[offset] |= (1 << bit_offset);
	}

	size_t get_free_memory_kb() {
		uint64_t freemem = 0;
		for (size_t i = 0; i < BLOCK_COUNT / bitsizeof(bitmap_type); i++) {
			for (uint8_t bit = 0; bit < bitsizeof(bitmap_type); bit++) {
				auto block_free = (bitmap[i] >> bit) & 0x1;
				if (block_free == 1)
					freemem++;
			}
		}
		return (freemem * BLOCK_SIZE) / 1024;
	}

}