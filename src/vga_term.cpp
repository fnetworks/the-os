#include "vga_term.hpp"
#include "port.hpp"

namespace cursor {

	static Port<uint8_t> cursor_command(0x3D4);
	static Port<uint8_t> cursor_data(0x3D5);

	void enable(uint8_t start, uint8_t end) {
		cursor_command.write(0x0A);
		cursor_data.write((cursor_command.read() & 0xC0) | start);

		cursor_command.write(0x0B);
		cursor_command.write((cursor_command.read() & 0xE0) | end);
	}

	void disable() {
		cursor_command.write(0x0A);
		cursor_data.write(0x20);
	}

	uint16_t get_cursor_position() {
		uint16_t pos = 0;
		cursor_command.write(0x0F);
		pos |= cursor_data.read();
		cursor_command.write(0x0E);
		pos |= ((uint16_t)cursor_data.read()) << 8;
		return pos;
	}

	void set_cursor_position(uint16_t pos) {
		cursor_command.write(0x0F);
		cursor_data.write((uint8_t)(pos & 0xFF));
		cursor_command.write(0x0E);
		cursor_data.write((uint8_t)((pos >> 8) & 0xFF));
	}

	inline uint16_t xy_to_index(uint16_t x, uint16_t y, uint16_t width) { return y * width + x; }

	struct pos {
		uint16_t x;
		uint16_t y;
	};

	inline pos index_to_xy(uint16_t index, uint16_t width) {
		uint16_t x = index % width;
		uint16_t y = index / width;
		return pos{x, y};
	}

};

VGATerminal VGATerminal::global_term = {};

VGATerminal::VGATerminal(uint16_t* buffer) : m_buffer(buffer) {
	cursor::pos p = cursor::index_to_xy(cursor::get_cursor_position(), width());
	m_y = p.x;
	m_x = p.y;
}

void VGATerminal::clear() {
	m_x = 0;
	m_y = 0;

	for (uint16_t y = 0; y < height(); y++) {
		for (uint16_t x = 0; x < width(); x++) {
			m_buffer[width() * y + x] = (m_buffer[width() * y + x] & 0xFF00) | ' ';
		}
	}

	cursor::set_cursor_position(cursor::xy_to_index(0, 0, width()));
}

void VGATerminal::new_line() {
	m_x = 0;
	m_y++;
	if (m_y >= height()) {
		clear();
	} else {
		cursor::set_cursor_position(cursor::xy_to_index(m_x, m_y, width()));
	}
}

void VGATerminal::put_char(char ch) {
	if (ch == '\r') {
	} else if (ch == '\n') {
		new_line();
	} else if (ch == '\t') {
		for (unsigned int i = 0; i < 4; i++)
			put_char(' ');
	} else {
		m_buffer[width() * m_y + m_x] = (m_buffer[width() * m_y + m_x] & 0xFF00) | ch;
		m_x++;
		if (m_x >= width())
			new_line();
		else
			cursor::set_cursor_position(cursor::xy_to_index(m_x, m_y, width()));
	}
}

void VGATerminal::put_string(const char* str) {
	for (int i = 0; str[i] != '\0'; i++)
		put_char(str[i]);
}