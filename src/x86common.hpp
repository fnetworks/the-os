#pragma once

#include <stdint.h>

namespace x86 {

	struct TablePointer {
		uint16_t limit;
		void* pointer;
	
		template<typename T>
		static constexpr TablePointer from(T* ptr, uint16_t table_size) {
			return TablePointer {.limit = static_cast<uint16_t>(table_size - 1), .pointer = ptr};
		}

		template<typename E>
		static constexpr TablePointer from_entries(E* ptr, uint16_t entries) {
			return from<E>(ptr, sizeof(E) * entries);
		}
	} __attribute__((packed));

};