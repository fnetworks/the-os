#pragma once

#include <stdint.h>

namespace iasm {

	/**
	 * ``cli`` instruction.
	 * Disables interrupts.
	 */
	__attribute__((always_inline)) inline void cli() { asm volatile("cli"); }
	/**
	 * ``sti`` instruction.
	 * Enables interrupts.
	 */
	__attribute__((always_inline)) inline void sti() { asm volatile("sti"); }
	/**
	 * ``hlt`` instruction.
	 * Halts the processor.
	 */
	__attribute__((always_inline)) inline void hlt() { asm volatile("hlt"); }
	/**
	 * ``int`` instruction.
	 * Generates an interrupt.
	 */
	__attribute__((always_inline)) inline void intr(uint16_t id) {
		asm volatile("int %0" ::"i"(id));
	}
	/**
	 * ``nop`` instruction.
	 * Does nothing.
	 */
	__attribute__((always_inline)) inline void nop() { asm volatile("nop"); }

	__attribute__((always_inline)) inline void set_esp(uint8_t* esp) {
		asm volatile ("mov %0, %%esp" : : "ri" (esp) : "memory");
	}

}