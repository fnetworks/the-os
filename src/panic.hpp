#pragma once

__attribute__((noreturn)) void panic();
__attribute__((noreturn)) void panic(const char* msg);