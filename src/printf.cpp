#include "printf.hpp"
#include "vga_term.hpp"
#include <stdint.h>
#include <stdarg.h>

void print(const char* str) {
	VGATerminal::current()->put_string(str);
}

void println() {
	VGATerminal::current()->new_line();
}

void println(const char* str) {
	auto* term = VGATerminal::current();
	term->put_string(str);
	term->new_line();
}

template<typename T>
void swap(T& a, T& b) {
	T c = a;
	a = b;
	b = c;
}

void reverse(char str[], int length) {
	int start = 0;
	int end = length - 1;
	while (start < end) {
		swap(*(str + start), *(str + end));
		start++;
		end--;
	}
}

char* itoa(int num, char* str, int base) {
	int i = 0;
	bool isNegative = false;

	if (num == 0) {
		str[i++] = '0';
		str[i] = '\0';
		return str;
	}

	if (num < 0 && base == 10) {
		isNegative = true;
		num = -num;
	}

	while (num != 0) {
		char rem = static_cast<char>(num % base);
		str[i++] = (rem > 9) ? (rem - 10) + 'a' : rem + '0';
		num = num / base;
	}

	if (isNegative)
		str[i++] = '-';

	str[i] = '\0';

	reverse(str, i);

	return str;
}

char* uint_to_string(unsigned int num, char* str, int base) {
	int i = 0;

	if (num == 0) {
		str[i++] = '0';
		str[i] = '\0';
		return str;
	}

	while (num != 0) {
		char rem = static_cast<char>(num % base);
		str[i++] = (rem > 9) ? (rem - 10) + 'a' : rem + '0';
		num = num / base;
	}

	str[i] = '\0';

	reverse(str, i);

	return str;
}

void printf(const char* fmt...) {
	va_list args;
	va_start(args, fmt);

	VGATerminal* term = VGATerminal::current();

	while (*fmt != '\0') {
		if (*fmt == '%') {
			fmt++;
			if (*fmt == 'd') {
				int i = va_arg(args, int);
				char str[100];
				term->put_string(itoa(i, str, 10));
			} else if (*fmt == 'u') {
				unsigned int i = va_arg(args, unsigned int);
				char str[100];
				term->put_string(uint_to_string(i, str, 10));
			} else if (*fmt == 'h') {
				int i = va_arg(args, int);
				char str[100];
				term->put_string(itoa(i, str, 16));
			} else if (*fmt == 'c') {
				int c = va_arg(args, int);
				term->put_char(static_cast<char>(c));
			} else if (*fmt == 's') {
				const char* c = va_arg(args, char*);
				term->put_string(c);
			} else if (*fmt == '%') {
				term->put_char('%');
			}
		} else {
			term->put_char(*fmt);
		}
		fmt++;
	}
}