#pragma once

#include "port.hpp"
#include <stdint.h>

class ComPort {
public:
	Port<uint8_t> data;
	Port<uint8_t> interrupt_enable;
	Port<uint8_t> divisor_low;
	Port<uint8_t> divisor_high;
	Port<uint8_t> int_id;
	Port<uint8_t> line_ctrl;
	Port<uint8_t> modem_ctrl;
	Port<uint8_t> line_status;
	Port<uint8_t> modem_status;
	Port<uint8_t> scratch;

public:
	ComPort(uint16_t addr) :
		data(addr + (uint16_t)0),
		interrupt_enable(addr + (uint16_t)1),
		divisor_low(addr + (uint16_t)0),
		divisor_high(addr + (uint16_t)1),
		int_id(addr + (uint16_t)2),
		line_ctrl(addr + (uint16_t)3),
		modem_ctrl(addr + (uint16_t)4),
		line_status(addr + (uint16_t)5),
		modem_status(addr + (uint16_t)6),
		scratch(addr + (uint16_t)7)
	{}

	inline void write_str(const char* str) {
		for (uint32_t i = 0; str[i] != '\0'; i++)
			data.write(str[i]);
	}
};

inline void comstuff() {
	// ComPort port(0x3F8);
	// port.interrupt_enable.write(0);
	// port.line_ctrl.write(0x80);
	// port.divisor_low.write(0x03);
	// port.divisor_high.write(0x00);
	// port.line_ctrl.write(0x03);
	// port.int_id.write(0xC7);
	// port.modem_ctrl.write(0x0B);
}