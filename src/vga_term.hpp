#pragma once

#include <stdint.h>

#define DEFAULT_TEXT_BUFFER_ADDRESS 0xB8000

enum class VGAColor : uint8_t {
	Black = 0,
	Blue = 1,
	Green = 2,
	Cyan = 3,
	Red = 4,
	Magenta = 5,
	Brown = 6,
	LightGray = 7,
	DarkGray = 8,
	LightBlue = 9,
	LightGreen = 10,
	LightCyan = 11,
	LightRed = 12,
	LightMagenta = 13,
	Yellow = 14,
	White = 15
};

class VGATerminal {
private:
	static VGATerminal global_term;

public:
	static inline VGATerminal* current() { return &global_term; }

private:
	uint16_t* m_buffer;
	uint16_t m_x, m_y;
	VGAColor fg, bg;
	bool available = false;

public:
	VGATerminal(uint16_t* buffer = reinterpret_cast<uint16_t*>(DEFAULT_TEXT_BUFFER_ADDRESS));

	void clear();
	void new_line();
	void put_char(char c);
	void put_string(const char* str);

	inline void set_foreground(VGAColor color) { fg = color; }
	inline void set_background(VGAColor color) { bg = color; }

	// TODO NOOO HARDCODING!!1!
	inline uint16_t width() const { return 80; };
	inline uint16_t height() const { return 25; };

	inline bool is_available() const { return true; }
};