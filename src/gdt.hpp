#pragma once

#include <stdint.h>

constexpr uint16_t get_segment_selector(uint16_t segment, uint8_t privilege_level = 0,
										bool ldt = false) {
	return segment << 3 | ((ldt ? 1 : 0) << 2) | (privilege_level & 0x3);
}

struct SegmentSelector {
private:
	uint16_t m_segment;
	uint8_t m_privilege_level : 3;
	bool m_ldt;

public:
	constexpr SegmentSelector(uint16_t segment, uint8_t privilege_level = 0, bool ldt = false)
		: m_segment(segment), m_privilege_level(privilege_level), m_ldt(ldt) {}

	constexpr uint16_t segment() const { return m_segment; }
	constexpr uint8_t privilege_level() const { return m_privilege_level; }
	constexpr bool local_table() const { return m_ldt; }

	constexpr operator uint16_t() const {
		return get_segment_selector(m_segment, m_privilege_level, m_ldt);
	}
};

struct SegmentDescriptor {
private:
	uint16_t m_limit_low;
	uint16_t m_base_low;
	uint8_t m_base_mid;
	uint8_t m_access;
	uint8_t m_limit_high : 4;
	uint8_t m_flags : 4;
	uint8_t m_base_high;

public:
	void fill(uint32_t base, uint32_t limit, uint8_t flags);
	void nullify();
	uint32_t base() const;
	uint32_t limit() const;
} __attribute__((packed));

class GlobalDescriptorTable {
private:
	SegmentDescriptor entries[3];

public:
	void setup_flat_kernel();
	void activate();

	SegmentSelector code_segment_selector() const;
	SegmentSelector data_segment_selector() const;
} __attribute__((packed));
