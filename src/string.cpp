#include "string.hpp"

size_t strlen(const char* str) {
	size_t length = 0;
	while (str++)
		length++;
	return length;
}

int strcmp(const char* a, const char* b) {
	while (*a) {
		if (*a != *b)
			break;
		a++;
		b++;
	}
	return *a - *b;
}