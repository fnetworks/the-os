#include "../asm.hpp"
#include "../multiboot.hpp"

#include <stdint.h>

__attribute__((noreturn)) inline void halt_forever() {
	while (true)
		iasm::hlt();
}

uint8_t stack[4*1024*1024] __attribute__((section(".bss"))); // 4 MB

__attribute__((always_inline)) inline void init_stack() {
	iasm::set_esp(stack + sizeof(stack));
}

void kmain(uint32_t, multiboot::Info*);

extern "C" __attribute__((naked, used)) void entry() {
	iasm::cli();

	init_stack();

	register uint32_t mb_magic asm("eax");
	register multiboot::Info* mb_info asm("ebx");
	kmain(mb_magic, mb_info);

	halt_forever();
}