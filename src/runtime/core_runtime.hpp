#pragma once

extern "C" void _init();
extern "C" void _fini();

inline void invoke_global_ctors() {
	_init();
}

inline void invoke_global_dtors() {
	_fini();
}
