.section .init
	/* constructor handling ends here */
	popl %ebp
	ret

.section .fini
	/* destructor handling ends here */
	popl %ebp
	ret
