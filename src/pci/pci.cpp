#include "pci.hpp"

#include "../printf.hpp"

PCIController::PCIController() : m_dataPort(0xCFC), m_commandPort(0xCF8) {}

inline uint32_t compute_pci_id(uint16_t bus, uint16_t device, uint16_t function,
							   uint32_t register_offset) {
	return (1 << 31) | ((bus & 0xFF) << 16) | ((device & 0x1F) << 11) | ((function & 0x07) << 8) |
		   (register_offset & 0xFC);
}

uint32_t PCIController::read(uint16_t bus, uint16_t device, uint16_t function,
							 uint32_t register_offset) {
	uint32_t id = compute_pci_id(bus, device, function, register_offset);
	m_commandPort.write(id);
	return m_dataPort.read() >> (8 * (register_offset % 4));
}

void PCIController::write(uint16_t bus, uint16_t device, uint16_t function,
						  uint32_t register_offset, uint32_t value) {
	uint32_t id = compute_pci_id(bus, device, function, register_offset);
	m_commandPort.write(id);
	m_dataPort.write(value);
}

bool PCIController::device_has_function(uint16_t bus, uint16_t device) {
	return read(bus, device, 0, 0x0E) & (1 << 7);
}

template<typename T>
inline void printhexpadright(T value, uint16_t fill) {
	printf("%h", value);
	int16_t pad = 0;
	if (value > 0xffffffff)
		pad = fill - 9;
	else if (value > 0xfffffff)
		pad = fill - 8;
	else if (value > 0xffffff)
		pad = fill - 7;
	else if (value > 0xfffff)
		pad = fill - 6;
	else if (value > 0xffff)
		pad = fill - 5;
	else if (value > 0xfff)
		pad = fill - 4;
	else if (value > 0xff)
		pad = fill - 3;
	else if (value > 0xf)
		pad = fill - 2;
	else
		pad = fill - 1;

	if (pad <= 0)
		return;
	for (uint16_t i = 0; i < (uint16_t)pad; i++) {
		printf(" ");
	}
}

void PCIController::enumerate_devices() {
	printf("== PCI devices ==\n");
	printf("BUS DEVICE FUNCTION VENDOR_ID  DEVICE_ID\n");
	for (uint16_t bus = 0; bus < 8; bus++) {
		for (uint16_t device = 0; device < 32; device++) {
			uint16_t functions = device_has_function(bus, device) ? 8 : 1;
			for (uint16_t function = 0; function < functions; function++) {

				uint16_t vendor_id = read(bus, device, function, 0x00);

				if (vendor_id == 0 || vendor_id == (uint16_t)-1)
					continue;

				uint16_t device_id = read(bus, device, function, 0x02);

				printhexpadright(bus, 4);
				printhexpadright(device, 7);
				printhexpadright(function, 9);
				printhexpadright(vendor_id, 11);
				printhexpadright(device_id, 11);

				println();
			}
		}
	}
}