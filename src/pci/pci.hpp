#pragma once

#include <stdint.h>

#include "../port.hpp"

class PCIController {
private:
	Port<uint32_t> m_dataPort;
	Port<uint32_t> m_commandPort;

public:
	PCIController();

	uint32_t read(uint16_t bus, uint16_t device, uint16_t function, uint32_t register_offset);
	void write(uint16_t bus, uint16_t device, uint16_t function, uint32_t register_offset, uint32_t value);

	bool device_has_function(uint16_t bus, uint16_t device);

	void enumerate_devices();
};