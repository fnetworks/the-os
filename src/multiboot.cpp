#include "multiboot.hpp"

static const multiboot::Header mb_header __attribute__((used, section(".multiboot"))) =
	multiboot::Header::create(multiboot::PAGE_ALIGN | multiboot::MEMORY_INFO);