#pragma once

#include <stdint.h>
#include <stddef.h>

#include "multiboot.hpp"

namespace pmm {

	void init(const multiboot::Info* mb_info);
	void* allocate();
	void mark_reserved(const void* address);
	void free(void* address);

	size_t get_free_memory_kb();

}