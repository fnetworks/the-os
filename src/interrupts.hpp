#pragma once

#include <stdint.h>
#include "port.hpp"
#include "gdt.hpp"

#define INTERRUPT_COUNT 256 // TODO -> 256


constexpr uint8_t EOI = 0x20;

struct InterruptFrame {
	uint32_t ip;
	uint32_t cs;
	uint32_t flags;
	uint32_t sp;
	uint32_t ss;
};

typedef void (*interrupt_handler)(uint8_t id, InterruptFrame* frame, uint32_t code);

class InterruptManager {
public:
	struct GateDescriptor {
		uint16_t handler_address_low;
		uint16_t gdt_code_segment_selector;
		uint8_t reserved;
		uint8_t access;
		uint16_t handler_address_high;
	} __attribute__((packed));

private:
	static GateDescriptor interruptDescriptorTable[INTERRUPT_COUNT];

	Port<uint8_t> pic_master_command;
	Port<uint8_t> pic_master_data;
	Port<uint8_t> pic_slave_command;
	Port<uint8_t> pic_slave_data;

	static void register_idt_entry(uint8_t interrupt, uint16_t code_segment, const void* handler, uint8_t privilege_level, uint8_t type);
	static void register_entries(uint16_t code_segment);

	interrupt_handler* handlers[INTERRUPT_COUNT];

	static InterruptManager* current_manager;
public:

	inline void send_end_of_interrupt(uint8_t interrupt_id) {
		if (interrupt_id >= 0x20 && interrupt_id <= 0x2f) {
			if (interrupt_id >= 0x28)
				pic_slave_command.write(EOI);
			pic_master_command.write(EOI);
		}
	}
	inline static InterruptManager* current() { return current_manager; }

	InterruptManager(GlobalDescriptorTable* gdt);

	// TODO private / friend
	void handle_interrupt(uint8_t id, InterruptFrame* frame, uint32_t code);

	void activate();
	void deactivate();

	void register_interrupt_handler(uint8_t interrupt, interrupt_handler* handler);
};