#include "runtime/core_runtime.hpp"
#include "gdt.hpp"
#include "interrupts.hpp"
#include "printf.hpp"
#include "vga_term.hpp"

#include "asm.hpp"
#include "panic.hpp"

#include "pmalloc.hpp"

#include "pci/pci.hpp"

void kmain(uint32_t mb_magic, multiboot::Info* mb_info) {
	invoke_global_ctors();

	if (mb_magic != multiboot::BOOTLOADER_MAGIC)
		panic("Got invalid bootloader magic");

	VGATerminal::current()->clear();

	if (mb_info->flags & multiboot::INFO_BOOT_LOADER_NAME)
		printf("Bootloader is %s\n", mb_info->boot_loader_name);
	else
		println("Bootloader is unknown: INFO_BOOT_LOADER_NAME flag not set");

	GlobalDescriptorTable gdt;
	gdt.setup_flat_kernel();
	gdt.activate();
	println("New GDT loaded");

	InterruptManager interrupts(&gdt);
	interrupts.activate();
	println("Interrupts active");

	pmm::init(mb_info);
	printf("Memory manager initialized. Free: %d KB\n", pmm::get_free_memory_kb());

	PCIController pci;
	pci.enumerate_devices();

	while (true) iasm::hlt();
}