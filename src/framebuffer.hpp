#pragma once

#include <stdint.h>

class FrameBuffer {
private:
	uint8_t* m_screen;
	uint32_t m_width;
	uint32_t m_height;
	uint32_t m_pitch;
	uint8_t m_bpp;

public:
	constexpr FrameBuffer(uint8_t* screen, uint32_t width, uint32_t height, uint32_t pitch, uint8_t bpp)
		: m_screen(screen), m_width(width), m_height(height), m_pitch(pitch), m_bpp(bpp) {}

	void put_pixel(uint32_t x, uint32_t y, uint8_t r, uint8_t g, uint8_t b) {
		m_screen[y * m_pitch + x * (m_bpp / 8) + 0] = r;
		m_screen[y * m_pitch + x * (m_bpp / 8) + 1] = g;
		m_screen[y * m_pitch + x * (m_bpp / 8) + 2] = b;
	}
};