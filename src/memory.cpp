#include "memory.hpp"
#include <stdint.h>

void* memset(void* ptr, int value, size_t num) {
	uint8_t* p = static_cast<uint8_t*>(ptr);
	while (num--)
		*p++ = static_cast<uint8_t>(value);
	return p;
}