#include "interrupts.hpp"
#include "asm.hpp"
#include "panic.hpp"
#include "printf.hpp"
#include "x86common.hpp"
#include "string.hpp"
#include "memory.hpp"

InterruptManager::GateDescriptor InterruptManager::interruptDescriptorTable[INTERRUPT_COUNT] = {};

InterruptManager* InterruptManager::current_manager = nullptr;

void InterruptManager::register_idt_entry(uint8_t interrupt, uint16_t gdt_code_segment,
									 const void* handler, uint8_t privilege_level, uint8_t type) {
	const uint8_t IDT_DESC_PRESENT = 0x80;

	interruptDescriptorTable[interrupt].handler_address_low = ((uint32_t)handler) & 0xFFFF;
	interruptDescriptorTable[interrupt].handler_address_high = ((uint32_t)handler >> 16) & 0xFFFF;
	interruptDescriptorTable[interrupt].gdt_code_segment_selector = gdt_code_segment;
	interruptDescriptorTable[interrupt].access =
		IDT_DESC_PRESENT | type | ((privilege_level & 3) << 5);
	interruptDescriptorTable[interrupt].reserved = 0;
}

const char* exceptions[32]{
	"Division by zero",
	"Debugger",
	"NMI",
	"Breakpoint",
	"Overflow",
	"Bounds",
	"Invalid Opcode",
	"Coprocessor Not Available",
	"Double Fault",
	"Coprocessor Segment Overrun",
	"Invalid Task State Segment",
	"Segment Not Present",
	"Stack Fault",
	"General Protection Fault",
	"Page Fault",
	"Reserved",
	"Math Fault",
	"Alignment Check",
	"Machine Check",
	"SIMD Floating Point Exception",
	"Reserved",
	"Reserved",
	"Reserved",
	"Reserved",
	"Reserved",
	"Reserved",
	"Reserved",
	"Reserved",
	"Reserved",
	"Reserved",
	"Reserved",
	"Reserved",
};

inline void lidt(InterruptManager::GateDescriptor* ptr, uint16_t entries) {
	auto idtr = x86::TablePointer::from_entries(ptr, entries);
	asm volatile("lidt %0" : : "m"(idtr));
}

InterruptManager::InterruptManager(GlobalDescriptorTable* gdt)
	: pic_master_command(0x20, true), pic_master_data(0x21, true), pic_slave_command(0xA0, true),
	  pic_slave_data(0xA1, true) {
	uint16_t code_segment = gdt->code_segment_selector();
	register_entries(code_segment);

	pic_master_command.write(0x11);
	pic_slave_command.write(0x11);

	pic_master_data.write(0x20);
	pic_slave_data.write(0x28);

	pic_master_data.write(0x04);
	pic_slave_data.write(0x02);

	pic_master_data.write(0x01);
	pic_slave_data.write(0x01);

	pic_master_data.write(0x00);
	pic_slave_data.write(0x00);
	println("Interrupt controllers reprogrammed");

	lidt(interruptDescriptorTable, INTERRUPT_COUNT);
	println("New interrupt table loaded");
}

void InterruptManager::activate() {
	if (current_manager != nullptr && current_manager != this)
		current_manager->deactivate();
	iasm::sti();
	current_manager = this;
}

void InterruptManager::deactivate() {
	if (this == current_manager) {
		iasm::cli();
		current_manager = nullptr;
	}
}

void InterruptManager::register_interrupt_handler(uint8_t interrupt, interrupt_handler* handler) {
	handlers[interrupt] = handler;
}

void InterruptManager::handle_interrupt(uint8_t id, InterruptFrame* frame, uint32_t code) {
	if (handlers[id]) {
		(*handlers[id])(id, frame, code);
		return;
	}
	if (id <= 0x1f) {
		printf("PANIC: Kernel received CPU exception: 0x%h (%s) with code 0x%h\n", id,
			   exceptions[id], code);
		panic();
	}
}

template<uint8_t ID>
__attribute__((interrupt)) void handle_irq(InterruptFrame* frame) {
	auto* manager = InterruptManager::current();
	if (manager != nullptr) { // TODO assert
		manager->handle_interrupt(ID, frame, 0);
		manager->send_end_of_interrupt(ID);
	}
}

template<uint8_t ID>
__attribute__((interrupt)) void handle_irq_err(InterruptFrame* frame, int code) {
	auto* manager = InterruptManager::current();
	if (manager != nullptr) { // TODO assert
		manager->handle_interrupt(ID, frame, code);
		manager->send_end_of_interrupt(ID);
	}
}

__attribute__((interrupt)) void irq_stub(InterruptFrame*) {
}


#define table_entry(id) (void*)(&handle_irq<id>)
#define table_entry_err(id) (void*)(&handle_irq_err<id>)

const void* routine_table[] = {
	table_entry(0x0),
	table_entry(0x1),
	table_entry(0x2),
	table_entry(0x3),
	table_entry(0x4),
	table_entry(0x5),
	table_entry(0x6),
	table_entry(0x7),
	table_entry_err(0x8),
	table_entry(0x9),
	table_entry_err(0xa),
	table_entry_err(0xb),
	table_entry_err(0xc),
	table_entry_err(0xd),
	table_entry_err(0xe),
	table_entry(0xf),
	table_entry(0x10),
	table_entry_err(0x11),
	table_entry(0x12),
	table_entry(0x13),
	table_entry(0x14),
	table_entry(0x15),
	table_entry(0x16),
	table_entry(0x17),
	table_entry(0x18),
	table_entry(0x19),
	table_entry(0x1a),
	table_entry(0x1b),
	table_entry(0x1c),
	table_entry(0x1d),
	table_entry_err(0x1e),
	table_entry(0x1f),
	table_entry(0x20),
	table_entry(0x21),
	table_entry(0x22),
	table_entry(0x23),
	table_entry(0x24),
	table_entry(0x25),
	table_entry(0x26),
	table_entry(0x27),
	table_entry(0x28),
	table_entry(0x29),
	table_entry(0x2a),
	table_entry(0x2b),
	table_entry(0x2c),
	table_entry(0x2d),
	table_entry(0x2e),
	table_entry(0x2f),
	table_entry(0x30),
	table_entry(0x31),
	table_entry(0x32)
};


constexpr uint8_t IDT_INTERRUPT_GATE = 0xE;

void InterruptManager::register_entries(uint16_t code_segment) {
	constexpr uint16_t handler_count = sizeof(routine_table) / sizeof(void*);

	for (uint8_t i = 0; i < handler_count; i++)
		register_idt_entry(i, code_segment, routine_table[i], 0, IDT_INTERRUPT_GATE);

	for (uint16_t i = handler_count; i < 256; i++)
		register_idt_entry(i, code_segment, reinterpret_cast<void*>(&irq_stub), 0, IDT_INTERRUPT_GATE);
}