#include "gdt.hpp"
#include "asm.hpp"
#include "x86common.hpp"

const uint32_t NULL_DESC = 0;
const uint32_t CODE_DESC = 1;
const uint32_t DATA_DESC = 2;

#define SEG_PRESENT (1 << 7)
#define SEG_PRIVILEGE(x) (x << 5)
#define SEG_TYPE_CODE_DATA (1 << 4)
#define SEG_EXECUTABLE (1 << 3)
#define SEG_DIRECTION_CONFORMING (1 << 2)
#define SEG_READWRITE (1 << 1)
#define SEG_ACCESSED (1)

void GlobalDescriptorTable::setup_flat_kernel() {
	entries[NULL_DESC].nullify();
	entries[CODE_DESC].fill(0, 64 * 1024 * 1024,
							SEG_PRESENT | SEG_PRIVILEGE(0) | SEG_TYPE_CODE_DATA | SEG_EXECUTABLE |
								SEG_READWRITE);
	entries[DATA_DESC].fill(0, 64 * 1024 * 1024,
							SEG_PRESENT | SEG_PRIVILEGE(0) | SEG_TYPE_CODE_DATA | SEG_READWRITE);
}

inline void set_cs(SegmentSelector selector) {
	if (__builtin_constant_p(selector)) {
		asm goto("ljmp %0, $%l1" ::"i"(static_cast<uint16_t>(selector)):"memory":jump_target);
	} else {
		struct {
			void* ptr;
			uint16_t segment;
		} __attribute__((packed)) pt = {
			.ptr = &&jump_target,
			.segment = static_cast<uint16_t>(selector)
		};
		asm goto ("ljmp *%0"::"m"(pt):"memory":jump_target);
		// The label is not actually used here, but the compiler seems to have problems without it
	}
jump_target:
	return;
}

#define set_segment_register(reg, segment)                                                         \
	asm volatile("mov %0, %%" #reg "s" ::"r"(static_cast<uint16_t>(segment)))

inline void set_ds(SegmentSelector segment) { set_segment_register(d, segment); }
inline void set_es(SegmentSelector segment) { set_segment_register(e, segment); }
inline void set_fs(SegmentSelector segment) { set_segment_register(f, segment); }
inline void set_gs(SegmentSelector segment) { set_segment_register(g, segment); }
inline void set_ss(SegmentSelector segment) { set_segment_register(s, segment); }

inline void lgdt(SegmentDescriptor* ptr, uint16_t entries) {
	auto table = x86::TablePointer::from_entries(ptr, entries);
	asm volatile("lgdt %0" ::"m"(table));
}

void GlobalDescriptorTable::activate() {
	lgdt(entries, 3);

	const SegmentSelector data_segment = data_segment_selector();
	set_ds(data_segment);
	set_es(data_segment);
	set_fs(data_segment);
	set_gs(data_segment);
	set_ss(data_segment);
	set_cs(code_segment_selector());
}

SegmentSelector GlobalDescriptorTable::data_segment_selector() const {
	return SegmentSelector(DATA_DESC);
}

SegmentSelector GlobalDescriptorTable::code_segment_selector() const {
	return SegmentSelector(CODE_DESC);
}

void SegmentDescriptor::nullify() {
	m_limit_low = 0;
	m_base_low = 0;
	m_base_mid = 0;
	m_flags = 0;
	m_limit_high = 0;
	m_access = 0;
	m_base_high = 0;
}

#define SEG_FLAG_GRANULAR (1 << 3)
#define SEG_FLAG_SIZE (1 << 2)

void SegmentDescriptor::fill(uint32_t base, uint32_t limit, uint8_t flags) {
	m_flags |= SEG_FLAG_SIZE;
	if (limit > 65536) {
		if ((limit & 0xFFF) != 0xFFF) {
			limit = (limit >> 12) - 1;
		} else {
			limit = limit >> 12;
		}
		m_flags |= SEG_FLAG_GRANULAR;
	}

	m_limit_low = limit & 0xFFFF;
	m_limit_high = (limit >> 16) & 0xF;

	m_base_low = base & 0xFFFF;
	m_base_mid = (base >> 16) & 0xFF;
	m_base_high = (base >> 24) & 0xFF;

	m_access = flags;
}

uint32_t SegmentDescriptor::base() const {
	uint32_t result = m_base_high;
	result = (result << 8) | m_base_mid;
	result = (result << 16) | m_base_low;
	return result;
}

uint32_t SegmentDescriptor::limit() const {
	const uint8_t* target = reinterpret_cast<const uint8_t*>(this);
	uint32_t result = target[6] & 0xF;
	result = (result << 8) | target[1];
	result = (result << 8) | target[0];

	if ((target[6] & 0xC0) == 0xC0)
		result = (result << 12) | 0xFFF;

	return result;
}