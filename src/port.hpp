#pragma once

#include <stdint.h>
#include "asm.hpp"

template<typename T>
class Port {
private:
	const uint16_t m_port;
	const bool m_delay;

public:
	constexpr Port(uint16_t port, bool delay = false);

	inline T read();
	inline void write(T value);
};

template<typename T>
constexpr Port<T>::Port(uint16_t port, bool delay) : m_port(port), m_delay(delay) {}

inline void delay() {
	for (int i = 0; i < 5; i++)
		iasm::nop();
}

// uint8_t
template<>
inline uint8_t Port<uint8_t>::read() {
	uint8_t result;
	asm volatile ("inb %1, %0" : "=a" (result) : "Nd" (m_port));
	return result;
}

template<>
inline void Port<uint8_t>::write(uint8_t value) {
	asm volatile ("outb %0, %1" : : "a" (value), "Nd" (m_port));
	if (m_delay)
		delay();
}


// uint16_t
template<>
inline uint16_t Port<uint16_t>::read() {
	uint16_t result;
	asm volatile ("inw %1, %0" : "=a" (result) : "Nd" (m_port));
	return result;
}

template<>
inline void Port<uint16_t>::write(uint16_t value) {
	asm volatile ("outw %0, %1" : : "a" (value), "Nd" (m_port));
	if (m_delay)
		delay();
}


// uint32_t
template<>
inline uint32_t Port<uint32_t>::read() {
	uint32_t result;
	asm volatile ("inl %1, %0" : "=a" (result) : "Nd" (m_port));
	return result;
}

template<>
inline void Port<uint32_t>::write(uint32_t value) {
	asm volatile ("outl %0, %1" : : "a" (value), "Nd" (m_port));
	if (m_delay)
		delay();
}