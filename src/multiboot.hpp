/*
 *  This file is a port of the multiboot.h file provided by the official multiboot specification.
 * It is adapted to use C++ features like namespaces and constexpr.
 */

#pragma once

// Replacement for multiboot_uint*_t types
#include <stdint.h>

/**
 * Structures related to the multiboot specification.
 */
namespace multiboot {
	/** How many bytes from the start of the file we search for the header. */
	constexpr uint32_t SEARCH = 8192;
	constexpr uint32_t HEADER_ALIGN = 4;

	/** The magic field should contain this. */
	constexpr uint32_t HEADER_MAGIC = 0x1BADB002;

	/** This should be in %eax. */
	constexpr uint32_t BOOTLOADER_MAGIC = 0x2BADB002;

	/** Alignment of multiboot modules. */
	constexpr uint32_t MOD_ALIGN = 0x00001000;

	/** Alignment of the multiboot info structure. */
	constexpr uint32_t INFO_ALIGN = 0x00000004;

	/* Flags set in the ’flags’ member of the multiboot header. */

	/** Align all boot modules on i386 page (4KB) boundaries. */
	constexpr uint32_t PAGE_ALIGN = 0x00000001;

	/** Must pass memory information to OS. */
	constexpr uint32_t MEMORY_INFO = 0x00000002;

	/** Must pass video information to OS. */
	constexpr uint32_t VIDEO_MODE = 0x00000004;

	/** This flag indicates the use of the address fields in the header. */
	constexpr uint32_t AOUT_CLUDGE = 0x00010000;

	/* Flags to be set in the ’flags’ member of the multiboot info structure. */

	/** is there basic lower/upper memory information? */
	constexpr uint32_t INFO_MEMORY = 0x00000001;
	/** is there a boot device set? */
	constexpr uint32_t INFO_BOOTDEV = 0x00000002;
	/** is the command-line defined? */
	constexpr uint32_t INFO_CMDLINE = 0x00000004;
	/** are there modules to do something with? */
	constexpr uint32_t INFO_MODS = 0x00000008;

	/* These next two are mutually exclusive */

	/** is there a symbol table loaded? */
	constexpr uint32_t AOUT_SYMS = 0x00000010;
	/** is there an ELF section header table? */
	constexpr uint32_t INFO_ELF_SHDR = 0x00000020;

	/** is there a full memory map? */
	constexpr uint32_t INFO_MEM_MAP = 0x00000040;

	/** Is there drive info? */
	constexpr uint32_t INFO_DRIVE_INFO = 0x00000080;

	/** Is there a config table? */
	constexpr uint32_t INFO_CONFIG_TABLE = 0x00000100;

	/** Is there a boot loader name? */
	constexpr uint32_t INFO_BOOT_LOADER_NAME = 0x00000200;

	/** Is there a APM table? */
	constexpr uint32_t INFO_APM_TABLE = 0x00000400;

	/** Is there video information? */
	constexpr uint32_t INFO_VBE_INFO = 0x00000800;
	constexpr uint32_t INFO_FRAMEBUFFER_INFO = 0x00001000;

	struct Header {
		/** Must be MULTIBOOT_MAGIC - see above. */
		uint32_t magic;

		/** Feature flags. */
		uint32_t flags;

		/** The above fields plus this one must equal 0 mod 2^32. */
		uint32_t checksum;

		/** These are only valid if MULTIBOOT_AOUT_KLUDGE is set. */
		uint32_t header_addr;
		uint32_t load_addr;
		uint32_t load_end_addr;
		uint32_t bss_end_addr;
		uint32_t entry_addr;

		/** These are only valid if MULTIBOOT_VIDEO_MODE is set. */
		uint32_t mode_type;
		uint32_t width;
		uint32_t height;
		uint32_t depth;

		constexpr void update_checksum() { checksum = -(magic + flags); }

		static constexpr Header create(uint32_t flags) {
			Header header = {};
			header.magic = HEADER_MAGIC;
			header.flags = flags;
			header.update_checksum();
			return header;
		}
	};

	/** The symbol table for a.out. */
	struct AOUTSymbolTable {
		uint32_t tabsize;
		uint32_t strsize;
		uint32_t addr;
		uint32_t reserved;
	};

	/** The section header table for ELF. */
	struct ELFSectionHeaderTable {
		uint32_t num;
		uint32_t size;
		uint32_t addr;
		uint32_t shndx;
	};

	struct Info {
		/** Multiboot info version number */
		uint32_t flags;

		/** Available memory from BIOS */
		uint32_t mem_lower;
		uint32_t mem_upper;

		/** "root" partition */
		uint32_t boot_device;

		/** Kernel command line */
		uint32_t cmdline;
		constexpr char* cmdline_ptr() const { return reinterpret_cast<char*>(cmdline); }

		/** Boot-Module list */
		uint32_t mods_count;
		uint32_t mods_addr;

		union {
			AOUTSymbolTable aout_sym;
			ELFSectionHeaderTable elf_sec;
		} u;

		/** Memory Mapping buffer */
		uint32_t mmap_length;
		uint32_t mmap_addr;

		/** Drive Info buffer */
		uint32_t drives_length;
		uint32_t drives_addr;

		/** ROM configuration table */
		uint32_t config_table;

		/** Boot Loader Name */
		uint32_t boot_loader_name;

		/** APM table */
		uint32_t apm_table;

		/** Video */
		/**
		 * The physical address of VBE control information returned by the VBE Function 00h.
		 */
		uint32_t vbe_control_info;
		/**
		 * The physical address of VBE mode information returned by the VBE Function 01h.
		 */
		uint32_t vbe_mode_info;
		/**
		 * The current video mode in the format specified in VBE 3.0
		 */
		uint16_t vbe_mode;
		uint16_t vbe_interface_seg;
		uint16_t vbe_interface_off;
		uint16_t vbe_interface_len;

		struct {
			/**
			 * Contains framebuffer physical address. This field is 64-bit wide but bootloader should
			 * set it under 4 GiB if possible for compatibility with kernels which aren’t aware of PAE
			 * or AMD64.
			 */
			uint64_t addr;
			constexpr void* addr_ptr() const { return reinterpret_cast<void*>(addr); }

			/**
			 * Contains the framebuffer pitch in bytes.
			 */
			uint32_t pitch;
			/**
			 * Contains the framebuffer width in pixels.
			 */
			uint32_t width;
			/**
			 * Contains the framebuffer height in pixels.
			 */
			uint32_t height;
			/**
			 * Contains the number of bits per pixel.
			 */
			uint8_t bpp;

			enum class FramebufferType : uint8_t { Indexed = 0, RGB = 1, EGAText = 2 };
			FramebufferType type;

			union {
				struct {
					uint32_t framebuffer_palette_addr;
					uint16_t framebuffer_palette_num_colors;
				};
				struct {
					uint8_t framebuffer_red_field_position;
					uint8_t framebuffer_red_mask_size;
					uint8_t framebuffer_green_field_position;
					uint8_t framebuffer_green_mask_size;
					uint8_t framebuffer_blue_field_position;
					uint8_t framebuffer_blue_mask_size;
				};
			};
		} framebuffer;
	};

	struct Color {
		uint8_t red;
		uint8_t green;
		uint8_t blue;
	};

	struct MMapEntry {
		uint32_t size;
		uint64_t addr;
		uint64_t len;

		enum class Type : uint32_t {
			Unknown = 0,
			Available = 1,
			Reserved = 2,
			ACPIReclaimable = 3,
			NVS = 4,
			BadRAM = 5
		};

		Type type;
	} __attribute__((packed));

	struct ModuleList {
		/** the memory used goes from bytes ’mod_start’ to ’mod_end-1’ inclusive */
		uint32_t mod_start;
		uint32_t mod_end;

		/** Module command line */
		uint32_t cmdline;

		/** padding to take it to 16 bytes (must be zero) */
		uint32_t pad;
	};

	/** APM BIOS info. */
	struct APMInfo {
		uint16_t version;
		uint16_t cseg;
		uint32_t offset;
		uint16_t cseg_16;
		uint16_t dseg;
		uint16_t flags;
		uint16_t cseg_len;
		uint16_t cseg_16_len;
		uint16_t dseg_len;
	};

}