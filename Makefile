CXX := toolchain/bin/i686-elf-g++
LD := toolchain/bin/i686-elf-gcc
AS := toolchain/bin/i686-elf-as

LDFLAGS := -nostdlib -lgcc

CXXFLAGS := -std=c++17 -ffreestanding -fno-exceptions -fno-rtti -fno-use-cxa-atexit -fno-leading-underscore

WARNINGS := -Wall -Wextra -Wshadow -Wpointer-arith -Wcast-align \
            -Wwrite-strings \
            -Wredundant-decls -Winline -Wno-long-long
# TODO add -pedantic when multiboot structure doesn't contain anonymous structs anymore
# TODO add -Wconversion
CXXFLAGS += $(WARNINGS)

ifeq ($(profile),release)
CXXFLAGS += -O3 -s -flto
LDFLAGS += -s -flto
BINDIR=bin/release
else
CXXFLAGS += -Og -g
BINDIR=bin/debug
endif

OUTPUT=$(BINDIR)/kernel.bin
INT_DIR=$(BINDIR)/intermediate
ISODIR=$(BINDIR)/iso

CPP_SOURCES := $(shell find src/ -name '*.cpp')

CRTBEGIN_OBJ := $(shell $(CXX) $(CXXFLAGS) -print-file-name=crtbegin.o)
CRTI_OBJ := $(INT_DIR)/crti.o
CRTN_OBJ := $(INT_DIR)/crtn.o
CRTEND_OBJ := $(shell $(CXX) $(CXXFLAGS) -print-file-name=crtend.o)

GCCLIBDIR := $(dir $(shell $(CXX) $(CXXFLAGS) -print-file-name=libgcc.a))

OBJECTS=$(CRTI_OBJ) $(CRTBEGIN_OBJ) $(patsubst src/%.cpp,$(INT_DIR)/%.cpp.o,$(CPP_SOURCES)) $(CRTEND_OBJ) $(CRTN_OBJ)

all: $(OUTPUT)

clean:
	rm -rf $(BINDIR)/

run: $(OUTPUT)
	qemu-system-i386 -kernel $< -serial stdio

run-iso: $(BINDIR)/theos.iso
	qemu-system-i386 -cdrom $< -serial stdio -device rtl8139

debug: $(OUTPUT)
	qemu-system-i386 -kernel $< -serial stdio -s -S &
	gdb $< -x gdbrc

debug-iso: $(BINDIR)/theos.iso
	qemu-system-i386 -cdrom $< -serial stdio -s -S &
	gdb $(OUTPUT) -x gdbrc

iso: $(BINDIR)/theos.iso

$(BINDIR)/theos.iso: $(ISODIR)/boot/kernel.bin $(ISODIR)/boot/grub/grub.cfg
	grub-mkrescue $(ISODIR) -o $@

# TODO cp should be replaced with some form of link that xorriso supports
$(ISODIR)/boot/kernel.bin: $(OUTPUT)
	@mkdir -p $(ISODIR)/boot
	cp -f $< $@

$(ISODIR)/boot/grub/grub.cfg: grub.cfg
	@mkdir -p $(ISODIR)/boot/grub
	cp -f $< $@

$(OUTPUT): $(OBJECTS) link.ld
	$(LD) -T link.ld -o $@ $(OBJECTS) -L$(GCCLIBDIR) $(LDFLAGS)
	grub-file --is-x86-multiboot $@

$(INT_DIR)/interrupts.cpp.o: src/interrupts.cpp
	@mkdir -p $(INT_DIR)
	$(CXX) -o $@ $(CXXFLAGS) -mgeneral-regs-only -MMD -MP -c $<
$(INT_DIR)/interrupt_handlers.cpp.o: src/interrupt_handlers.cpp
	@mkdir -p $(INT_DIR)
	$(CXX) -o $@ $(CXXFLAGS) -mgeneral-regs-only -MMD -MP -c $<

$(INT_DIR)/%.cpp.o: src/%.cpp
	@mkdir -p $(shell dirname $@)
	$(CXX) -o $@ $(CXXFLAGS) -MMD -MP -c $<

$(CRTN_OBJ): src/runtime/crtn.s
	@mkdir -p $(INT_DIR)
	$(AS) -o $@ $<

$(CRTI_OBJ): src/runtime/crti.s
	@mkdir -p $(INT_DIR)
	$(AS) -o $@ $<

.PHONY: all

-include $(DEPFILES)